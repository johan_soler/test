@extends('main')

{{-- VISTA DE LOGIN --}}
@section('content')
    <div class="content card w-75 mx-auto mt-5 pt-3 text-center">
        <div class="card-title">
            <h1>TIC TAC TOE</h1>
            <hr>
        </div>
        <div class="card-body">
            <div class="">
                <p class="fs-6 fst-italic">
                    Para iniciar el juego abre la web en un navegador y selecciona "Iniciar Juego" para generar un ID, un amigo tuyo debera abrir la web en otro navegador y darle a "Conectarse al juego" e ingresar tu ID
                </p>
                <div class="row w-25 mx-auto gy-3">
                    <button id="startGame" class="col-12 btn btn-outline-success">
                        Iniciar Juego
                    </button>
                    <button id="connectGame" class="col-12 btn btn-outline-info" name="conectar" value="true">
                        Conectarse al Juego
                    </button>
                </div>
            </div>
        </div>
        <div class="card-footer text-muted">
            <p> V:1.0 by Johan Soler</p>
        </div>
    </div>

@endsection

{{-- SCRIPT NECESARIO PARA DAR ACCIONES A LOS BOTONES Y MANEJAR CODIGOS --}}
@push('script')
    <script>
        (function(){
            //ADD THE GAMECODE AND MOVE TO GAME
            $("#connectGame").on('click', function(){
                var code = window.prompt('Ingresa el codigo');
                console.log("the code is" + code)
                window.location.replace('game/2/' + code+'/1')
            })

            //GET THE GAMECODE AND MOVE TO GAME
            $("#startGame").on('click', function(){
                var newCode = Math.floor(Math.random() * (9999 - 1000) + 1000);
                console.log(newCode);
                window.location.replace('game/1/' + newCode+'/1')
            })
        })();

    </script>
@endpush