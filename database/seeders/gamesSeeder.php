<?php

namespace Database\Seeders;

use App\Models\Game;
use Illuminate\Database\Seeder;

class gamesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $game = new Game();

        $game->gameCode = '0000';
        $game->player1 = 'Nicolas';
        $game->player2 = 'Alison';
        $game->winner = 'Alison';

        $game->save();

        $game = new Game();

        $game->gameCode = '0001';
        $game->player1 = 'jugador 1';
        $game->player2 = 'jugador 2';
        $game->winner = 'jugador 1';

        $game->save();

        $game = new Game();

        $game->gameCode = '0002';
        $game->player1 = 'Nicolas';
        $game->player2 = 'Alison';
        $game->winner = 'Nicolas';

        $game->save();
    }
}
