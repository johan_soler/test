# README #

Instrucctivo de uso:

### Instalacion ###

Una vez realizada la clonacion del repo tendremos que:
Primero nos tendremos que asegurar de crear el archivo .env (se puede copiar el .env.example para usarlo de modelo)
luego tendremos que agregar o modificar las siguientes variables **importante que aparezcan las siguientes variables tal como estan**:

DB_CONNECTION=mysql

DB_HOST=127.0.0.1

DB_PORT=3306

DB_DATABASE=test

DB_USERNAME=root

DB_PASSWORD=



FIREBASE_API_KEY=AIzaSyDkV-8iIE2HTpCfNziRTArSdGGelnEQo2U

FIREBASE_AUTH_DOMAIN=tictactoe-1b3d2.firebaseapp.com

FIREBASE_DATABASE_URL=https://tictactoe-1b3d2-default-rtdb.firebaseio.com/

FIREBASE_PROJECT_ID=tictactoe-1b3d2

FIREBASE_STORAGE_BUCKET=tictactoe-1b3d2.appspot.com

FIREBASE_MESSAGING_SENDER_ID=912028082505

FIREBASE_APP_ID=1:912028082505:web:a55dd38bcb8813aa8d7a98



__(enlace firebase actualizado por ultima vez:05/02/2022 - habilitado)__

una vez tengamos esta informacion en nuestro .env y nuestro gestar de bases de datos MySQL corriendo, crearemos una base de datos llamada 'test'
posteriormente desde la terminal del proyecto ejecutaremos en orden los siguientes comandos esperando que la consola termine de procesar 

**composer install --ignore-platform-req=ext-sodium**

**php artisan migrate:fresh --seed**

**php artisan serve**
_al ejecutar este ultimo puede salir un mensaje de parte de laravel mencionando la appKay, solo dar en la opcion "generar" y recargar la web_


### Como funciona? ###
 
ingrsar siempre desde el enlace raiz por ejemplo _http://localhost:8000/_ (se recomienda tener este enlace en el porta papeles)
una vez ingrese encontrara dos botones, con el primero incia el juego y le dara el codigo en la parte superior izquierda,
con ese codigo desde otra ventana o navegador, podra darle al segundo boton y en la ventana emergente ingresar el numero

una vez ambos usuarios esten en linea, se iran turnando segun el semaforo de color les diga "Tu turno" o "Esperando"
_se recomienda tener paciencia y darle tiempo a la pagina que cargue_

una vez se decida el ganador, en la ventana del jugador que incio la sala aparecera el ganador
_cualquier pregunta por favor comunicarla al correo: johanicolas2001@gmail.com_