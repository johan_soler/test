<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    //CONSTRUCTOR INICIAL DEL JUEGO
    public function __invoke($player, $code, $turno)
    {
        $this->code = $code; 
        $this->code = $player; 
        $this->turno = $turno; 
        return view('game', ['player' => $player, 'code' => $code,'turno' => $turno, 'initGame' => 1]);
    }

    public function save($code, $player1, $player2, $winner)
    {
        $game = new Game;

        $game->gameCode = $code;
        $game->player1 = $player1;
        $game->player2 = $player2;
        $game->winner = $winner;

        $game->save();
        echo $game;
    }
}
